﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Playground/Actions/Audio")]
public class AudioAction : Action
{
    public AudioClip soundToPlay;

    public void Start()
    {
        if (soundToPlay == null)
        {
            Debug.LogError("Failed to play audio - no sound clip provided for object " + name);
        }
    }

    public override bool ExecuteAction(GameObject dataObject)
    {
        if (soundToPlay != null)
        {
            AudioSource.PlayClipAtPoint(soundToPlay, transform.position);
            return true;
        }
        else
        {
            return false;
        }
    }
}
