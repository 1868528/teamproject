﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

[AddComponentMenu("Playground/Actions/Add Points")]
public class AddPointsAction : Action
{
    [Header("AddPointsAction")]
    public int pointsToAdd = 1;
    
    public enum AllocationType
    {
        SPECIFIED, THIS_OBJECT, TRIGGERING_OBJECT
    }

    [Tooltip("How the player getting the points is chosen")]
    public AllocationType playerChoice;

    [Range(1,2)]
    [ShowOnEnum("playerChoice",(int)AllocationType.SPECIFIED)]
    public int playerID;


    private UIScript userInterface;



    private void Start()
    {
        // Find the UI in the scene and store a reference for later use
        userInterface = GameObject.FindObjectOfType<UIScript>();
    }



    public override bool ExecuteAction(GameObject dataObject)
    {
        if (userInterface != null)
        {
            int playerNumber = 0;

            switch (playerChoice)
            {
                case AllocationType.SPECIFIED:
                    playerNumber = playerID - 1;
                    break;
                case AllocationType.THIS_OBJECT:
                    if (gameObject.CompareTag("Player"))
                    {
                        playerNumber = 0;
                    }
                    else
                    {
                        playerNumber = 1;
                    }
                    break;
                case AllocationType.TRIGGERING_OBJECT:
                    if (dataObject.CompareTag("Player"))
                    {
                        playerNumber = 0;
                    }
                    else
                    {
                        playerNumber = 1;
                    }
                    break;
            }

            userInterface.AddPoints(playerNumber, pointsToAdd);

            return true;
        }
        else
        {
            Debug.LogWarning("User Interface prefab has not been found in the scene. The action can't execute!");
            return false;
        }
    }



}
